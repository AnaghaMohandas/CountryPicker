import XCTest

import CountryPickerTests

var tests = [XCTestCaseEntry]()
tests += CountryPickerTests.allTests()
XCTMain(tests)
